# copyright: 2018, The Authors

title "Satellite Endpoints Check"

# Main Url Check
control "test-01" do
  impact 0.9
  title "Main Url Check"
  desc "Checking the availability of the Main Url"
  describe http("http://main.techcrumble.cloud") do
    its('status') { should cmp 200 }
  end
end
#Green Url Check
control "test-02" do
  impact 0.5
  title "green Url Check"
  desc "Checking the availability of the Main Url"
  describe http("http://green.techcrumble.cloud") do
    its('status') { should cmp 200 }
  end
end
#Blue Url Check
control "test-03" do
  impact 0.9
  title "blue Url Check"
  desc "Checking the availability of the Main Url"
  describe http("http://blue.techcrumble.cloud") do
    its('status') { should cmp 200 }
  end
end
